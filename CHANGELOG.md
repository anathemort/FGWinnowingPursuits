# FGU Winnowing Pursuits
## Changelog

### 2.3.2
* Updated for compatibility with FGU 4.4.8

### 2.3.1
* Fixed 4E Powers interaction

### 2.3.0
* Compatibility with collapsible containers as provided by [Extraplanar Containers](https://forge.fantasygrounds.com/shop/items/13/view)
  * Winnowing Pursuits will show results for items that are inside collapsed containers, but respect the collapsed state when the search is cleared
* Compatibility with [3.5E/PFRPG Advanced Charsheet](https://forge.fantasygrounds.com/shop/items/861/view)

### 2.2.1
* Fixed 4E Powers interaction

### 2.2.0
* Dynamic power/spell schools, which adds filter options based on discovered schools (thanks **@MeAndUnique**!)
* Support for PF2 "Concentrate" trait

### 2.1.0
* Added exclusive Potion and Scroll inventory filters (thanks **@kevininrussia**!)

### 2.0.1
* Resolved adventure logs compatibility when using [Friend Zone](https://forge.fantasygrounds.com/shop/items/170/view)

### 2.0.0
* Renamed extension to **Winnowing Pursuits**
* Added search and filters to the actions/powers tab

### 1.5.0
* Added "Inventory Search Filters" section to client options to control visible filter options
* Added "Wondrous Item" to 4E Magical filter

### 1.4.2
* Fixed 4E Ritual filter

### 1.4.1
* Added better support for 4E filters (magic items, ritual, adventuring gear) (thanks **@kevininrussia**!)  

### 1.4.0
* Added logs search (5E)
* Compatibility with [FloatingTabs](https://www.fantasygrounds.com/forums/showthread.php?67847-5E-Floating-Character-Sheet-Tabs) (thanks **@ScriedRaven**!)
* Increased versatility of filters: magical now includes potions, goods and services includes tools and more

### 1.3.0
* Added inventory filter (thanks **@daddyogreman**!)
* Resolved party sheet search sync (thanks **@Trenloe**!)
* Resolved inventory search sharing input values (thanks **@Trenloe**!)

### 1.2.0
* Added party sheet inventory search

### 1.1.0
* Added support for PFRPG, PFRPG2, 3.5E, and 4E rulesets
